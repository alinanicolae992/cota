COTA
=====
**Co**gnitive **T**raining for people suffering from **A**lzheimer’s

## Table of contents
- 🛴 [Code structure](#Code structure)  
- 🗿[Description](#description)
- 🏓 [How to Play](#how-to-play)  
- 🤓 [Installation](#installation)  
- 🖼 [Screenshots](#screenshots)  
- 🚀 [Motivation](#motivation)  
- 💾 [Technical Details](#technical-details)  
- 🎓 [Course Project](#course-project)  

## Code structure
The application consist of the executable class CotaApp.java which is in charge of creating our model view and controller classes.
Besides these 4 clases we have a CotaHand class which acts as our Hand reference, we work with 2 instances of it for left and right hands of the user.  
  
This application does a lot of drawing. Given our reuse of drawn objects moving around in the screen we create a CotaFigure class, an abstract class which each of the actual figures (CotaCircle, CotraTriangle,..., CotaOctagon) extend.
Most of the game properties are kept in our model (CotaGameModel) while the game logic is kept in the controller (CotaKinect), which in turns requests redraws from the view (CotaGameView). The view object extends a library class called OpenGLPanel which includes a few useful methods relevant to Kinect interaction.  
  
Kinect library refers to tracked user bodies as _Skeletons_. It provides a drawing method for the skeleton which offers a quick and dirty way to see an _sticky man_ figure on an openGL canvas (the view). We did some customization on this method to get it to behave as we wanted. In particular, we had to invert the X-axis values to obtain our desired mirror image.

## Description
COTA is a cognitive training system that helps people in the early stages of Alzheimer’s disease to train their memory skills. The system represents a gamified learning experience meant to ease the cognitive training. The exercise is projected on a screen/wall, the user's body is detected and tracked by a Kinect device which the system the displays on the display device.

## How to Play
The purpose of the game is to remember the position of different objects on screen and put them in the right place. For example, 2 objects (a triangle and a circle) will be shown on the screen in different locations for 10 seconds. You have to memorize the position and shape of the objects. After the 10 seconds lapse, the objects will disappear. Now you have to drag the correct objects from the bottom menu (with different types of objects) and put them in the correct position. If you guess it right, you pass to the next level.

## Installation   
### Option 1: From source (recommended)
* Clone this repo
* Download `j4k-examples.zip` file from https://research.dwi.ufl.edu/ufdw/download.php
* Extract folder and copy lib subdirectory into this repo
* Copy also the DLLs for your machine (either 32 or 64bit), there's 2 per architechture, one is for Kinectv1 and the other for Kinectv2. Copy both in case you don't know which one is for you.
* Import this project from eclipse as File > Import > General > Projects from Folder or Archive
* Run from Eclipse

### Option 2: Download and run full JAR 
* Requires Java8 or later
* Download our release JAR from [here](https://drive.google.com/file/d/1qaKPly_jahn6RHqWNY_GFajZ3G9yKo7X/view?usp=sharing).


## Screenshots
Screenshot 1: Game start screen  
![alt text](res/screen4.png)  
Screenshot 2: Holding 2 figures, one on each hand  
![alt text](res/screen2.png)  
Screenshot 3: Removing an item from the hand  
![alt text](res/screen3.png)  
Screenshot 4: Placing objects in the target location  
![alt text](res/screen5.png)  
Screenshot 5: User dancing  
![alt text](res/screen1.png)  


## Motivation
Alzheimer’s disease is the most common cause of dementia and almost 50 million people suffer from it. Much research has been done to see if cognitive training helps prevent Alzheimer’s disease or at least prolong the early stages. Even though the results are promising on the tested subjects, there is still much research to be done until saying with absolute certainty it has a positive impact. Nevertheless, the potential is visible.


## Technical details
This applicaton uses J4KSDK. J4SDK is a Kinect's Java binding library developed at Univeristy of Florida, Digital Worlds Institute. More information on their [website](https://research.dwi.ufl.edu/ufdw/j4k/index.php).


## Course Project
 Course Project for [HCI909 Advanced Programming of Interactive Systems](https://perso.telecom-paristech.fr/eagan/class/advUI/)  
@ Université Paris-Sud   
Prof: James Eagan  
T.A.: Yujiro Okuya  
Master Student: Alina Nicolae  
Master Student: Joel Gil  