package cota;

import javax.media.opengl.GL2;

import javafx.geometry.Point3D;

/**
 * Implements a Rectangle in OpenGL
 *
 */
public class CotaRectangle extends CotaFigure {

	private Point3D centralPoint;
	private float radius;
	private float[] colorRGB;
	private float canvasPosX;
	private float canvasPosY;
	private float canvasPosZ;
	private float width;
	private float height;

	public CotaRectangle(Point3D position, float width, float height) {
		this.width = width;
		this.height = height;
		radius = (this.width > this.height) ? this.height : this.width;
		radius /= 2;
		setPosition(position);
		colorRGB = CotaFigure.ORANGE;
	}
	
	public float getWidth() {
		return width;
	}
	
	public float getHeight() {
		return height;
	}

	@Override
	public void draw(GL2 gl) {
		// top left
		float x = canvasPosX - (width / 2);
		float  y = canvasPosY + (height / 2);
		float z = canvasPosZ;

		gl.glBegin(GL2.GL_TRIANGLES);

		gl.glVertex3f(x, y, z); // top left
		gl.glVertex3f(x, y - height, z); // bottom left
		gl.glVertex3f(x + width, y, z); // top right

		gl.glVertex3f(x + width, y, z); // top right
		gl.glVertex3f(x, y - height, z); // bottom left
		gl.glVertex3f(x + width, y - height, z); // bottom right

		gl.glEnd();
	}

	@Override
	public Point3D getPosition() {
		return centralPoint;
	}

	@Override
	public void setPosition(Point3D position) {
		centralPoint = position;
		canvasPosX = (float) centralPoint.getX();
		canvasPosY = (float) centralPoint.getY();
		canvasPosZ = (float) -centralPoint.getZ();
	}

	@Override
	public float getRadius() {
		return radius;
	}

	@Override
	public float[] getPositionOnCanvas() {
		float[] canvasPosition = { canvasPosX, canvasPosY, canvasPosZ };
		return canvasPosition;
	}

	@Override
	public void translateFigure(GL2 gl, Point3D position, float offsetX, float offsetY, float offsetZ) {
		gl.glTranslatef((float) position.getX() + offsetX, (float) position.getY() + offsetY,
				(float) position.getZ() + offsetZ);
	}

	@Override
	public void setColor(float red, float green, float blue) {
		colorRGB[0] = red;
		colorRGB[1] = green;
		colorRGB[2] = blue;
	}

	@Override
	public float[] getColor() {
		return colorRGB;
	}
	
	@Override
	public int getShape() {
		return CotaFigure.RECTANGLE;
	}

}
