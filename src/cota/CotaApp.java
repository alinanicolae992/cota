package cota;

import edu.ufl.digitalworlds.gui.DWApp;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Main class - runs the app
 *
 */

@SuppressWarnings("serial")
public class CotaApp extends DWApp {
	CotaKinect controller;
	JLabel accelerometer;

	@Override
	public void GUIsetup(JPanel p_root) {
		CotaGameModel model = new CotaGameModel();
		CotaGameView view = new CotaGameView();
		controller = new CotaKinect(model, view);
		view.setController(controller);

		if (System.getProperty("os.arch").toLowerCase().indexOf("64") < 0) {
			if (DWApp.showConfirmDialog("Performance Warning",
					"<html><center><br>WARNING: You are running a 32bit version of Java.<br>This may reduce significantly the performance of this application.<br>It is strongly adviced to exit this program and install a 64bit version of Java.<br><br>Do you want to exit now?</center>"))
				System.exit(0);
		}

		setLoadingProgress("Intitializing Kinect...", 20);

		if (!controller.start(
				CotaKinect.DEPTH | CotaKinect.COLOR | CotaKinect.SKELETON | CotaKinect.XYZ | CotaKinect.PLAYER_INDEX)) {
			DWApp.showErrorDialog("ERROR",
					"<html><center><br>ERROR: The Kinect device could not be initialized.<br><br>1. Check if the Microsoft's Kinect SDK was succesfully installed on this computer.<br> 2. Check if the Kinect is plugged into a power outlet.<br>3. Check if the Kinect is connected to a USB port of this computer.</center>");
		}

		setLoadingProgress("Intitializing OpenGL...", 60);
		view.setShowVideo(false);
		p_root.add(view, BorderLayout.CENTER);
	}

	public static void main(String args[]) {

		createMainFrame("Kinect Viewer App");
		app = new CotaApp();
		setFrameSize(900, 700, null);
	}
}
