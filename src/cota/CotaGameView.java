package cota;

import java.util.ArrayList;
import javax.media.opengl.GL2;
import com.jogamp.opengl.util.gl2.GLUT;
import edu.ufl.digitalworlds.j4k.Skeleton;
import edu.ufl.digitalworlds.opengl.OpenGLPanel;
import javafx.geometry.Point3D;

/**
 * View class
 *
 * Draws the objects on screen every frame
 *
 */

@SuppressWarnings("serial")
public class CotaGameView extends OpenGLPanel {
	
	boolean show_video;
	private CotaKinect cotaController;
	private String message; // message to draw on screen
	private int frame;
	private int countdownFrame;
	private boolean levelMessage; // true at the start of a new level -> for displaying a message
	
	public CotaGameView() {
		frame = 0;
		countdownFrame = 0;
		show_video = false;
		levelMessage = false;
	}

	public void setController(CotaKinect controller) {
		cotaController = controller;
	}
	
	/**
	 * When a game is about to start, a countdown (from 3 to 1) is displayed on the
	 * view.
	 * 
	 * @param gl
	 */
	public void drawStartGame(GL2 gl) {
		if (countdownFrame < 20)
			drawTextMessage(gl, "Game starts in 3...", new Point3D(-0.4, 0.8, -2));
		if (countdownFrame >= 20 && countdownFrame < 40)
			drawTextMessage(gl, "Game starts in 2...", new Point3D(-0.4, 0.8, -2));
		if (countdownFrame >= 40 && countdownFrame < 60)
			drawTextMessage(gl, "Game starts in 1...", new Point3D(-0.4, 0.8, -2));
	}

	@Override
	public void draw() {
		GL2 gl = getGL2();
		drawRoomWalls(gl);
		drawMenuHolder(gl);
		drawMenuShapes(gl);
		drawRemovalObject(gl);
		drawStartGame(gl);
		drawLevel(gl);
		drawMirrorSkeletons(gl);
		drawHandObjects(gl);
		if (levelMessage) {
			drawTextMessage(gl, message, new Point3D(-0.4, 0.8, -2));
		}
		drawTargetsAchieved(gl);
		drawEndGame(gl);
		++countdownFrame;
		++frame;
	}

	/**
	 * This method draws the room "walls", we are hoping this will help the depth
	 * effect and serve as some type of guidance for the user. We are still not sure
	 * if these walls help or hurt the UI.
	 * 
	 * @param gl
	 */
	private void drawRoomWalls(GL2 gl ) {
		float ratio = (float) this.getHeight() / (float )this.getWidth();
		float xSize = 1.5f;
		float ySize = xSize * ratio;
		
		gl.glColor3f(0, 0, 0);
		for (float z = 0; z > -8f; z=z-0.5f) {
			gl.glBegin(GL2.GL_QUADS);
			gl.glVertex3f(xSize, ySize, z);
			gl.glVertex3f(xSize, ySize, z-0.25f);
			gl.glVertex3f(xSize, -ySize, z-0.25f);
			gl.glVertex3f(xSize, -ySize, z);
			gl.glEnd();
			
			gl.glBegin(GL2.GL_QUADS);
			gl.glVertex3f(xSize, -ySize, z-0.25f);
			gl.glVertex3f(xSize, -ySize, z);
			gl.glVertex3f(-xSize, -ySize, z);
			gl.glVertex3f(-xSize, -ySize, z-0.25f);
			gl.glEnd();
			
			gl.glBegin(GL2.GL_QUADS);
			gl.glVertex3f(-xSize, -ySize, z);
			gl.glVertex3f(-xSize, -ySize, z-0.25f);
			gl.glVertex3f(-xSize, ySize, z-0.25f);
			gl.glVertex3f(-xSize, ySize, z);
			gl.glEnd();
			
			gl.glBegin(GL2.GL_QUADS);
			gl.glVertex3f(-xSize, ySize, z-0.25f);
			gl.glVertex3f(-xSize, ySize, z);
			gl.glVertex3f(xSize, ySize, z);
			gl.glVertex3f(xSize, ySize, z-0.25f);
			gl.glEnd();
		}
	}

	public void setLevelMessage(boolean status, String message) {
		levelMessage = status;
		this.message = message;
	}
	
	/**
	 * Display target items on screen for the current level
	 * @param gl
	 */
	public void drawLevel(GL2 gl) {
		if (cotaController.getLevel() == 1) {
			if (frame < 60 + cotaController.getTimeLimit()) {
				drawTargetItems(gl);
			}
		} else if (frame < cotaController.getTimeLimit()) {
			drawTargetItems(gl);
		}
	}

	/**
	 * J4SDK does something funny, it reads the data from the sensor and then
	 * negates the X axis such that the image is not a mirror reflection when drawn
	 * with its native method. We had to re-implement the method by re-negating the
	 * values coming from the Kinect. Possible future solution is to contribute to
	 * their library by adding a similar method in their Kinect class.
	 * 
	 * @param gl
	 */
	private void drawMirrorSkeletons(GL2 gl) {
		gl.glColor3f(1, 0, 0);
		gl.glLineWidth(6f);
		for (Skeleton skeleton : cotaController.getSkeletons()) {
			if (skeleton != null) {
				if (skeleton.isTracked()) {
					// 1 MAIN BODY: HIP_CENTER, SPINE, SHOULDER_CENTER, HEAD
					gl.glBegin(GL2.GL_LINES);

					if (skeleton.isJointTrackedOrInferred(Skeleton.SPINE_BASE)
							&& skeleton.isJointTrackedOrInferred(Skeleton.SPINE_MID)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.SPINE_BASE),
								skeleton.get3DJointY(Skeleton.SPINE_BASE), -skeleton.get3DJointZ(Skeleton.SPINE_BASE));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.SPINE_MID),
								skeleton.get3DJointY(Skeleton.SPINE_MID), -skeleton.get3DJointZ(Skeleton.SPINE_MID));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.NECK)
							&& skeleton.isJointTrackedOrInferred(Skeleton.SPINE_MID)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.SPINE_MID),
								skeleton.get3DJointY(Skeleton.SPINE_MID), -skeleton.get3DJointZ(Skeleton.SPINE_MID));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.NECK), skeleton.get3DJointY(Skeleton.NECK),
								-skeleton.get3DJointZ(Skeleton.NECK));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.NECK)
							&& skeleton.isJointTrackedOrInferred(Skeleton.HEAD)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.NECK), skeleton.get3DJointY(Skeleton.NECK),
								-skeleton.get3DJointZ(Skeleton.NECK));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HEAD), skeleton.get3DJointY(Skeleton.HEAD),
								-skeleton.get3DJointZ(Skeleton.HEAD));
					}

					gl.glEnd();

					// 2 LEFT ARM: SkeletonSHOULDER_CENTER, Skeleton.SHOULDER_LEFT,
					// Skeleton.ELBOW_LEFT, Skeleton.WRIST_LEFT, Skeleton.HAND_LEFT
					gl.glBegin(GL2.GL_LINES);

					if (skeleton.isJointTrackedOrInferred(Skeleton.NECK)
							&& skeleton.isJointTrackedOrInferred(Skeleton.SHOULDER_LEFT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.NECK), skeleton.get3DJointY(Skeleton.NECK),
								-skeleton.get3DJointZ(Skeleton.NECK));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.SHOULDER_LEFT),
								skeleton.get3DJointY(Skeleton.SHOULDER_LEFT),
								-skeleton.get3DJointZ(Skeleton.SHOULDER_LEFT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.SHOULDER_LEFT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.ELBOW_LEFT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.SHOULDER_LEFT),
								skeleton.get3DJointY(Skeleton.SHOULDER_LEFT),
								-skeleton.get3DJointZ(Skeleton.SHOULDER_LEFT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.ELBOW_LEFT),
								skeleton.get3DJointY(Skeleton.ELBOW_LEFT), -skeleton.get3DJointZ(Skeleton.ELBOW_LEFT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.ELBOW_LEFT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.WRIST_LEFT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.ELBOW_LEFT),
								skeleton.get3DJointY(Skeleton.ELBOW_LEFT), -skeleton.get3DJointZ(Skeleton.ELBOW_LEFT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.WRIST_LEFT),
								skeleton.get3DJointY(Skeleton.WRIST_LEFT), -skeleton.get3DJointZ(Skeleton.WRIST_LEFT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.WRIST_LEFT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.HAND_LEFT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.WRIST_LEFT),
								skeleton.get3DJointY(Skeleton.WRIST_LEFT), -skeleton.get3DJointZ(Skeleton.WRIST_LEFT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HAND_LEFT),
								skeleton.get3DJointY(Skeleton.HAND_LEFT), -skeleton.get3DJointZ(Skeleton.HAND_LEFT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.HAND_TIP_LEFT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.HAND_LEFT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HAND_TIP_LEFT),
								skeleton.get3DJointY(Skeleton.HAND_TIP_LEFT),
								-skeleton.get3DJointZ(Skeleton.HAND_TIP_LEFT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HAND_LEFT),
								skeleton.get3DJointY(Skeleton.HAND_LEFT), -skeleton.get3DJointZ(Skeleton.HAND_LEFT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.THUMB_LEFT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.HAND_LEFT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.THUMB_LEFT),
								skeleton.get3DJointY(Skeleton.THUMB_LEFT), -skeleton.get3DJointZ(Skeleton.THUMB_LEFT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HAND_LEFT),
								skeleton.get3DJointY(Skeleton.HAND_LEFT), -skeleton.get3DJointZ(Skeleton.HAND_LEFT));
					}

					gl.glEnd();

					// 3 RIGHT ARM: Skeleton.SHOULDER_CENTER, Skeleton.SHOULDER_RIGHT,
					// Skeleton.ELBOW_RIGHT, Skeleton.WRIST_RIGHT, Skeleton.HAND_RIGHT
					gl.glBegin(GL2.GL_LINES);

					if (skeleton.isJointTrackedOrInferred(Skeleton.NECK)
							&& skeleton.isJointTrackedOrInferred(Skeleton.SHOULDER_RIGHT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.NECK), skeleton.get3DJointY(Skeleton.NECK),
								-skeleton.get3DJointZ(Skeleton.NECK));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.SHOULDER_RIGHT),
								skeleton.get3DJointY(Skeleton.SHOULDER_RIGHT),
								-skeleton.get3DJointZ(Skeleton.SHOULDER_RIGHT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.SHOULDER_RIGHT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.ELBOW_RIGHT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.SHOULDER_RIGHT),
								skeleton.get3DJointY(Skeleton.SHOULDER_RIGHT),
								-skeleton.get3DJointZ(Skeleton.SHOULDER_RIGHT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.ELBOW_RIGHT),
								skeleton.get3DJointY(Skeleton.ELBOW_RIGHT),
								-skeleton.get3DJointZ(Skeleton.ELBOW_RIGHT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.ELBOW_RIGHT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.WRIST_RIGHT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.ELBOW_RIGHT),
								skeleton.get3DJointY(Skeleton.ELBOW_RIGHT),
								-skeleton.get3DJointZ(Skeleton.ELBOW_RIGHT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.WRIST_RIGHT),
								skeleton.get3DJointY(Skeleton.WRIST_RIGHT),
								-skeleton.get3DJointZ(Skeleton.WRIST_RIGHT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.WRIST_RIGHT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.HAND_RIGHT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.WRIST_RIGHT),
								skeleton.get3DJointY(Skeleton.WRIST_RIGHT),
								-skeleton.get3DJointZ(Skeleton.WRIST_RIGHT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HAND_RIGHT),
								skeleton.get3DJointY(Skeleton.HAND_RIGHT), -skeleton.get3DJointZ(Skeleton.HAND_RIGHT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.HAND_TIP_RIGHT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.HAND_RIGHT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HAND_TIP_RIGHT),
								skeleton.get3DJointY(Skeleton.HAND_TIP_RIGHT),
								-skeleton.get3DJointZ(Skeleton.HAND_TIP_RIGHT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HAND_RIGHT),
								skeleton.get3DJointY(Skeleton.HAND_RIGHT), -skeleton.get3DJointZ(Skeleton.HAND_RIGHT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.THUMB_RIGHT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.HAND_RIGHT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.THUMB_RIGHT),
								skeleton.get3DJointY(Skeleton.THUMB_RIGHT),
								-skeleton.get3DJointZ(Skeleton.THUMB_RIGHT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HAND_RIGHT),
								skeleton.get3DJointY(Skeleton.HAND_RIGHT), -skeleton.get3DJointZ(Skeleton.HAND_RIGHT));
					}

					gl.glEnd();

					// 4 LEFT LEG: Skeleton.HIP_CENTER, Skeleton.HIP_LEFT, Skeleton.KNEE_LEFT,
					// Skeleton.ANKLE_LEFT, Skeleton.FOOT_LEFT
					gl.glBegin(GL2.GL_LINES);
					if (skeleton.isJointTrackedOrInferred(Skeleton.SPINE_BASE)
							&& skeleton.isJointTrackedOrInferred(Skeleton.HIP_LEFT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.SPINE_BASE),
								skeleton.get3DJointY(Skeleton.SPINE_BASE), -skeleton.get3DJointZ(Skeleton.SPINE_BASE));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HIP_LEFT), skeleton.get3DJointY(Skeleton.HIP_LEFT),
								-skeleton.get3DJointZ(Skeleton.HIP_LEFT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.HIP_LEFT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.KNEE_LEFT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HIP_LEFT), skeleton.get3DJointY(Skeleton.HIP_LEFT),
								-skeleton.get3DJointZ(Skeleton.HIP_LEFT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.KNEE_LEFT),
								skeleton.get3DJointY(Skeleton.KNEE_LEFT), -skeleton.get3DJointZ(Skeleton.KNEE_LEFT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.KNEE_LEFT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.ANKLE_LEFT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.KNEE_LEFT),
								skeleton.get3DJointY(Skeleton.KNEE_LEFT), -skeleton.get3DJointZ(Skeleton.KNEE_LEFT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.ANKLE_LEFT),
								skeleton.get3DJointY(Skeleton.ANKLE_LEFT), -skeleton.get3DJointZ(Skeleton.ANKLE_LEFT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.ANKLE_LEFT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.FOOT_LEFT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.ANKLE_LEFT),
								skeleton.get3DJointY(Skeleton.ANKLE_LEFT), -skeleton.get3DJointZ(Skeleton.ANKLE_LEFT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.FOOT_LEFT),
								skeleton.get3DJointY(Skeleton.FOOT_LEFT), -skeleton.get3DJointZ(Skeleton.FOOT_LEFT));
					}
					gl.glEnd();

					// 5 RIGHT LEG: Skeleton.HIP_CENTER, Skeleton.HIP_RIGHT, Skeleton.KNEE_RIGHT,
					// Skeleton.ANKLE_RIGHT, Skeleton.FOOT_RIGHT
					gl.glBegin(GL2.GL_LINES);
					if (skeleton.isJointTrackedOrInferred(Skeleton.SPINE_BASE)
							&& skeleton.isJointTrackedOrInferred(Skeleton.HIP_RIGHT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.SPINE_BASE),
								skeleton.get3DJointY(Skeleton.SPINE_BASE), -skeleton.get3DJointZ(Skeleton.SPINE_BASE));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HIP_RIGHT),
								skeleton.get3DJointY(Skeleton.HIP_RIGHT), -skeleton.get3DJointZ(Skeleton.HIP_RIGHT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.HIP_RIGHT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.KNEE_RIGHT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.HIP_RIGHT),
								skeleton.get3DJointY(Skeleton.HIP_RIGHT), -skeleton.get3DJointZ(Skeleton.HIP_RIGHT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.KNEE_RIGHT),
								skeleton.get3DJointY(Skeleton.KNEE_RIGHT), -skeleton.get3DJointZ(Skeleton.KNEE_RIGHT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.KNEE_RIGHT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.ANKLE_RIGHT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.KNEE_RIGHT),
								skeleton.get3DJointY(Skeleton.KNEE_RIGHT), -skeleton.get3DJointZ(Skeleton.KNEE_RIGHT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.ANKLE_RIGHT),
								skeleton.get3DJointY(Skeleton.ANKLE_RIGHT),
								-skeleton.get3DJointZ(Skeleton.ANKLE_RIGHT));
					}
					if (skeleton.isJointTrackedOrInferred(Skeleton.ANKLE_RIGHT)
							&& skeleton.isJointTrackedOrInferred(Skeleton.FOOT_RIGHT)) {
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.ANKLE_RIGHT),
								skeleton.get3DJointY(Skeleton.ANKLE_RIGHT),
								-skeleton.get3DJointZ(Skeleton.ANKLE_RIGHT));
						gl.glVertex3f(skeleton.get3DJointX(Skeleton.FOOT_RIGHT),
								skeleton.get3DJointY(Skeleton.FOOT_RIGHT), -skeleton.get3DJointZ(Skeleton.FOOT_RIGHT));
					}
					gl.glEnd();

				}
			}
		}
	}
	
	public void resetFrame() {
		frame = 0;
	}
	
	/**
	 * Displays message on screen
	 * @param gl
	 * @param message
	 * @param position
	 */
	public void drawTextMessage(GL2 gl, String message, Point3D position) {
		GLUT glutty = new GLUT();
		gl.glColor3f(0.19f, 0.66f, 0.72f);
		gl.glRasterPos3d(position.getX(), position.getY(), position.getZ());
		glutty.glutBitmapString(GLUT.BITMAP_TIMES_ROMAN_24, message);
		
	}

	private void drawEndGame(GL2 gl) {
		if (levelMessage && cotaController.isGameOver()) {
			drawTextMessage(gl, message, new Point3D(-0.4, 0.8, -2));
		}
	}

	/**
	 * Draw the objects that are in the hands of the user if any
	 * @param gl
	 */
	private void drawHandObjects(GL2 gl) {
		if (cotaController.getRightHand() != null) {
			if (!cotaController.getRightHand().isEmpty()) {
				cotaController.getRightHand().getItemOnHand().draw(gl);
			}
		}

		if (cotaController.getLeftHand() != null) {
			if (!cotaController.getLeftHand().isEmpty()) {
				cotaController.getLeftHand().getItemOnHand().draw(gl);
			}
		}
	}

	private void drawTargetItems(GL2 gl) {
		for(CotaFigure target : cotaController.getTargetItems()) {
			gl.glColor3fv(target.getColor(), 0);
			target.draw(gl);
		}
	}

	/**
	 * Draw the trash bin objects
	 * @param gl
	 */
	private void drawRemovalObject(GL2 gl) {
		for (CotaFigure obj : cotaController.getRemovalObjects()) {
			gl.glColor3f(0.45f, 0.11f, 0.04f);
			obj.draw(gl);
		}
	}
	
	/**
	 * Draw the objects that the user has positioned correctly on screen
	 * @param gl
	 */
	private void drawTargetsAchieved(GL2 gl) {
		for(int i = 0; i < cotaController.getTargetItems().size(); ++i) {
			if (cotaController.isTargetAchieved(i)) {
				gl.glColor3fv(cotaController.getTargetItems().get(i).getColor(), 0);
				cotaController.getTargetItems().get(i).draw(gl);
			}
		}
	}

	public void setShowVideo(boolean flag) {
		show_video = flag;
	}

	/**
	 * Setup of OpenGL drawing space
	 */
	public void setup() {
		GL2 gl = getGL2();
		gl.glEnable(GL2.GL_COLOR_MATERIAL);
		background(1, 1, 1);
	}

	/**
	 * Draw the background squares of the bottom menu
	 * @param gl
	 */
	public void drawMenuHolder(GL2 gl) {
		gl.glColor3f(0.97f, 0.84f, 0.47f);
		for (CotaFigure background : cotaController.getMenuBackgrounds()) {
			background.draw(gl);
		}
	}

	/**
	 * Draw the shapes in the bottom menu
	 * @param gl
	 */
	private void drawMenuShapes(GL2 gl) {
		ArrayList<CotaFigure> shapesList = cotaController.getMenuShapes();
		for (int i = 0; i < shapesList.size(); i++) {
			switch (i) {
			case 0:
				gl.glColor3fv(CotaFigure.FOREST_GREEN, 0);
				break;
			case 1:
				gl.glColor3fv(CotaFigure.ICE_BLUE, 0);
				break;
			case 2:
				gl.glColor3fv(CotaFigure.TOMATO_RED, 0);
				break;
			case 3:
				gl.glColor3fv(CotaFigure.ORANGE, 0);
				break;
			case 4:
				gl.glColor3fv(CotaFigure.DEEP_BLUE, 0);
				break;
			default:
				gl.glColor3fv(CotaFigure.ICE_BLUE, 0);
				break;
			}
			shapesList.get(i).draw(gl);
		}
	}
}