package cota;

import javax.media.opengl.GL2;

import javafx.geometry.Point3D;

/**
 * Implements a Hexagon in OpenGL
 *
 */
public class CotaHexagon extends CotaFigure {

	private Point3D centralPoint;
	private float radius;
	private float[] colorRGB;
	private float canvasPosX;
	private float canvasPosY;
	private float canvasPosZ;

	public CotaHexagon(Point3D position, float radius) {
		this.radius = radius;
		setPosition(position);
		colorRGB = CotaFigure.TOMATO_RED;
	}

	@Override
	public void draw(GL2 gl) {
		gl.glBegin(GL2.GL_POLYGON);
		for (int i = 0; i < 6; ++i) {
			gl.glVertex3f(canvasPosX + radius * (float) Math.sin(i / 6.0 * 2 * 3.14),
					canvasPosY + radius * (float) Math.cos(i / 6.0 * 2 * 3.14), canvasPosZ);
		}
		gl.glEnd();
	}

	@Override
	public Point3D getPosition() {
		return centralPoint;
	}

	@Override
	public void setPosition(Point3D position) {
		centralPoint = position;
		canvasPosX = (float) centralPoint.getX();
		canvasPosY = (float) centralPoint.getY();
		canvasPosZ = (float) -centralPoint.getZ();
	}

	@Override
	public float getRadius() {
		return radius;
	}

	@Override
	public float[] getPositionOnCanvas() {
		float[] canvasPosition = { canvasPosX, canvasPosY, canvasPosZ };
		return canvasPosition;
	}

	@Override
	public void translateFigure(GL2 gl, Point3D position, float offsetX, float offsetY, float offsetZ) {
		gl.glTranslatef((float) position.getX() + offsetX, (float) position.getY() + offsetY,
				(float) position.getZ() + offsetZ);
	}

	@Override
	public void setColor(float red, float green, float blue) {
		colorRGB[0] = red;
		colorRGB[1] = green;
		colorRGB[2] = blue;
	}

	@Override
	public float[] getColor() {
		return colorRGB;
	}

	@Override
	public int getShape() {
		return CotaFigure.HEXAGON;
	}
}
