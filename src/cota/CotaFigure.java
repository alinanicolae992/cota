package cota;

import javax.media.opengl.GL2;

import javafx.geometry.Point3D;

/**
 * Abstract class defines general methods of a shape 
 *
 */
public abstract class CotaFigure {
	
	final static int CIRCLE 	= 1;
	final static int TRIANGLE 	= 2;
	final static int RECTANGLE = 3;
	final static int HEXAGON 	= 4;
	final static int OCTAGON 	= 5;
	final static int SHAPES_AVAILABLE = 5;
	final static float[] RED 	= {1.0f, 0.0f, 0.0f };
	final static float[] GREEN = {0.0f, 1.0f, 0.0f };
	final static float[] BLUE 	= {0.0f, 0.0f, 1.0f };
	final static float[] BLACK = {1.0f, 1.0f, 1.0f };
	final static float[] YELLOW = {1.0f, 1.0f, 0.0f };
	final static float[] PINK = {1.0f, 0.0f, 1.0f};
	final static float[] TURQUOISE = {0.0f, 1.0f, 1.0f};
	final static float[] WHITE = {0.0f, 0.0f, 0.0f };
	final static float[] FOREST_GREEN = {0.15f, 0.5f, 0.22f};
	final static float[] ICE_BLUE = {0.19f, 0.66f, 0.72f};
	final static float[] TOMATO_RED = {0.81f, 0.22f, 0.13f};
	final static float[] ORANGE = {1.0f, 0.48f, 0.28f};
	final static float[] DEEP_BLUE = {0.33f, 0.43f, 0.67f};
	
	abstract public void draw(GL2 gl);
	
	abstract public Point3D getPosition();
	
	abstract public void setPosition(Point3D position);
	
	abstract public float getRadius();
	
	abstract public float[] getPositionOnCanvas();
	
	abstract public void translateFigure(GL2 gl, Point3D position, float offsetX, float offsetY, float offsetZ);
	
	abstract public void setColor(float red, float green, float blue);
	
	abstract public float[] getColor();
	
	abstract public int getShape();
}

