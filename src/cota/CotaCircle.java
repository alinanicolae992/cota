package cota;

import javax.media.opengl.GL2;

import javafx.geometry.Point3D;

/**
 * Implements a Circle in OpenGL
 *
 */
public class CotaCircle extends CotaFigure {

	private Point3D centralPoint;
	private float radius;
	private float[] colorRGB;
	private float canvasPosX;
	private float canvasPosY;
	private float canvasPosZ;

	public CotaCircle(Point3D position, float radius) {
		this.radius = radius;
		setPosition(position);
		colorRGB = CotaFigure.DEEP_BLUE;
	}

	@Override
	public void draw(GL2 gl) {
		gl.glBegin(GL2.GL_POLYGON);
		for (int i = 0; i <= 300; i++) {
			double angle = 2 * Math.PI * i / 300;
			double x = Math.cos(angle);
			double y = Math.sin(angle);
			gl.glVertex3f(canvasPosX + radius * (float) x / 2, canvasPosY + radius * (float) y / 2, canvasPosZ);
		}
		gl.glEnd();
	}

	@Override
	public Point3D getPosition() {
		return centralPoint;
	}

	@Override
	public void setPosition(Point3D position) {
		centralPoint = position;
		canvasPosX = (float) centralPoint.getX();
		canvasPosY = (float) centralPoint.getY();
		canvasPosZ = (float) -centralPoint.getZ();
	}

	@Override
	public float[] getPositionOnCanvas() {
		float[] canvasPosition = { canvasPosX, canvasPosY, canvasPosZ };
		return canvasPosition;
	}

	@Override
	public void translateFigure(GL2 gl, Point3D position, float offsetX, float offsetY, float offsetZ) {
		gl.glTranslatef((float) position.getX() + offsetX, (float) position.getY() + offsetY,
				(float) position.getZ() + offsetZ);
	}

	@Override
	public void setColor(float red, float green, float blue) {
		colorRGB[0] = red;
		colorRGB[1] = green;
		colorRGB[2] = blue;
	}

	@Override
	public float[] getColor() {
		return colorRGB;
	}

	@Override
	public float getRadius() {
		return radius;
	}

	@Override
	public int getShape() {
		return CotaFigure.CIRCLE;
	}

}
