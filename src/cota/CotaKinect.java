package cota;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import edu.ufl.digitalworlds.j4k.J4KSDK;
import edu.ufl.digitalworlds.j4k.Skeleton;
import javafx.geometry.Point3D;

/**
 * Controller class
 * 
 * The connection between Model and View
 *
 */
public class CotaKinect extends J4KSDK {

	private CotaGameView cotaView;
	private CotaGameModel cotaModel;
	private Skeleton[] skeletons;
	private boolean gameOver;
	private int noLevels;

	public CotaKinect(CotaGameModel cotaModel, CotaGameView cotaView) {
		super();
		this.cotaModel = cotaModel;
		this.cotaView = cotaView;
		skeletons = new Skeleton[6];
		this.gameOver = false;
		this.noLevels = 4;
	}

	/**
	 * Returns the amount of time the figures will stay on the screen
	 * based on the current level
	 * @return
	 */
	public int getTimeLimit() {
		return 70 * cotaModel.getLevel();
	}

	public int getLevel() {
		return cotaModel.getLevel();
	}

	public void setViewer(CotaGameView viewer) {
		this.cotaView = viewer;
	}

	@Override
	public void onColorFrameEvent(byte[] color_frame) {}

	@Override
	public void onDepthFrameEvent(short[] depth_frame, byte[] body_index, float[] xyz, float[] uv) {}

	/*
	 * The following method will run every time a new skeleton frame is received
	 * from the Kinect sensor. The skeletons are converted into Skeleton objects.
	 */
	@Override
	public void onSkeletonFrameEvent(boolean[] flags, float[] positions, float[] orientations, byte[] state) {
		for (int i = 0; i < getSkeletonCountLimit(); i++) {
			skeletons[i] = Skeleton.getSkeleton(i, flags, positions, orientations, state, this);
			if (skeletons[i] != null) {
				if (skeletons[i].getTimesDrawn() <= 10 && skeletons[i].isTracked()) {
					// Right hand logic
					if (skeletons[i].isJointTrackedOrInferred(Skeleton.HAND_RIGHT)) {
						cotaModel.updateRightHand(skeletons[i].get3DJoint(Skeleton.HAND_RIGHT));
						if (cotaModel.getRightHand().isEmpty()) {
							checkHandOnMenu(cotaModel.getRightHand());
						} else {
							checkHandOnTargets(cotaModel.getRightHand());
							checkDeleteObject(cotaModel.getRightHand());
						}
					}
					
					// Left hand logic
					if (skeletons[i].isJointTrackedOrInferred(Skeleton.HAND_LEFT)) {
						cotaModel.updateLeftHand(skeletons[i].get3DJoint(Skeleton.HAND_LEFT));
						if (cotaModel.getLeftHand().isEmpty()) {
							checkHandOnMenu(cotaModel.getLeftHand());
						} else {
							checkHandOnTargets(cotaModel.getLeftHand());
							checkDeleteObject(cotaModel.getLeftHand());
						}
					}
					
					// Check if every object on screen is in the right position
					if (targetsCompleted()) {
						if (cotaModel.getLevel() == noLevels) {
							cotaView.setLevelMessage(true, "Congratulations! You won the game!");
							try {
								TimeUnit.SECONDS.sleep(1);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							cotaView.setLevelMessage(false, "");
							gameOver = true;
							cotaView.resetFrame();
							cotaModel.resetGame();

						} else {
							cotaView.setLevelMessage(true, "Good job! You go to level " + (cotaModel.getLevel() + 1));
							try {
								TimeUnit.SECONDS.sleep(1);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							cotaView.setLevelMessage(false, "");
							cotaModel.nextLevel();
							cotaView.resetFrame();
						}
					}
				}
			}
		}
		cotaView.draw();
	}

	/**
	 * All the items have been positioned in the right places
	 * @return
	 */
	private boolean targetsCompleted() {
		if (cotaModel.getNoTargetsAchieved() != cotaModel.getTargetItems().size())
			return false;
		for (boolean achieved : cotaModel.getTargetsAchieved()) {
			if (!achieved) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check if the hand is on any of the trash bin objects
	 * Called each frame
	 * @param hand
	 */
	private void checkDeleteObject(CotaHand hand) {
		for (CotaFigure obj : cotaModel.getRemovalObjects()) {
			if (checkCollision(hand.getPosition(), hand.getHandView().getRadius(), obj.getPosition(),
					obj.getRadius())) {
				hand.removeItemOnHand();
			}
		}
	}

	/**
	 * Check if the hand is on any of the targets
	 * Called each frame
	 * @param hand
	 */
	private void checkHandOnTargets(CotaHand hand) {
		int mapIndex = 0;
		while (!hand.isEmpty()) {
			CotaFigure targetFigure = cotaModel.getTargetItems().get(mapIndex);

			if (checkCollision(hand.getPosition(), hand.getItemOnHand().getRadius(), targetFigure.getPosition(),
					targetFigure.getRadius())) {
				if (hand.getItemOnHand().getShape() == targetFigure.getShape()
						&& !cotaModel.isTargetAchieved(mapIndex)) {
					hand.removeItemOnHand();
					cotaModel.setTargetsAchieved(mapIndex, true);
				}
			}
			++mapIndex;
			if (mapIndex >= cotaModel.getTargetItems().size())
				break;
		}
	}

	/**
	 * Check if the hand is on any of the shapes in the bottom menu
	 * Called each frame
	 * @param hand
	 */
	private void checkHandOnMenu(CotaHand hand) {
		CotaFigure figureCopy = null;
		for (CotaFigure figure : cotaModel.getShapesMenu()) {
			if (checkCollision(hand.getPosition(), hand.getHandView().getRadius(), figure.getPosition(),
					figure.getRadius())) {
				switch (figure.getShape()) {
				case CotaFigure.CIRCLE:
					figureCopy = new CotaCircle(hand.getPosition(), figure.getRadius());
					break;
				case CotaFigure.TRIANGLE:
					figureCopy = new CotaTriangle(hand.getPosition(), figure.getRadius());
					break;
				case CotaFigure.RECTANGLE:
					figureCopy = new CotaRectangle(hand.getPosition(), ((CotaRectangle) figure).getWidth(),
							((CotaRectangle) figure).getHeight());
					break;
				case CotaFigure.HEXAGON:
					figureCopy = new CotaHexagon(hand.getPosition(), figure.getRadius());
					break;
				case CotaFigure.OCTAGON:
					figureCopy = new CotaOctagon(hand.getPosition(), figure.getRadius());
					break;
				}
				hand.setItemOnHand(figureCopy);
				break;
			}
		}

	}

	/**
	 * Check the collision between two objects. Currently `radii` is used as a
	 * constant but could be used as a distance sum (as shown in in-line comment) to
	 * fine tune the accuracy needed to place items in their right spot
	 * 
	 * @param obj1
	 * @param radius1
	 * @param obj2
	 * @param radius2
	 * @return collision status
	 */
	private boolean checkCollision(Point3D obj1, float radius1, Point3D obj2, float radius2) {
		double radii = 0.2;
		double p1x = obj1.getX();
		double p1y = obj1.getY();
		double p1z = obj1.getZ();
		double p2x = obj2.getX();
		double p2y = obj2.getY();
		double p2z = obj2.getZ();
		double distance = Math
				.sqrt(((p1x - p2x) * (p1x - p2x)) + ((p1y - p2y) * (p1y - p2y)) + ((p1z - p2z) * (p1z - p2z)));
		return radii >= distance;
	}

	public Skeleton[] getSkeletons() {
		return skeletons;
	}

	public ArrayList<CotaFigure> getMenuBackgrounds() {
		return cotaModel.getShapesMenuBackground();
	}

	public ArrayList<CotaFigure> getMenuShapes() {
		return cotaModel.getShapesMenu();
	}

	public ArrayList<CotaFigure> getRemovalObjects() {
		return cotaModel.getRemovalObjects();
	}

	public CotaHand getRightHand() {
		return cotaModel.getRightHand();
	}

	public CotaHand getLeftHand() {
		return cotaModel.getLeftHand();
	}

	public ArrayList<CotaFigure> getTargetItems() {
		return cotaModel.getTargetItems();
	}

	public boolean isTargetAchieved(int index) {
		return cotaModel.isTargetAchieved(index);
	}

	public boolean isGameOver() {
		return gameOver;
	}

}
