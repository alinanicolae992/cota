package cota;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javafx.geometry.Point3D;

/**
 * Implements a Triangle in OpenGL
 *
 */
public class CotaTriangle extends CotaFigure {

	private Point3D centralPoint;
	private float radius;
	private float[] colorRGB;
	private float canvasPosX;
	private float canvasPosY;
	private float canvasPosZ;

	public CotaTriangle(Point3D position, float radius) {
		this.radius = radius;
		setPosition(position);
		colorRGB = CotaFigure.ICE_BLUE;
	}

	@Override
	public void draw(GL2 gl) {
		float sideSize = (float) ((6 * radius) / Math.sqrt(3));
		float median = (float) ((Math.sqrt(3) * sideSize) / 2);
		float medianSection = median / 3f;
		
		gl.glBegin(GL.GL_TRIANGLES);
		gl.glVertex3f(canvasPosX, canvasPosY + (medianSection) * 2, canvasPosZ);
		gl.glVertex3f(canvasPosX + (sideSize / 2f), canvasPosY - (medianSection), canvasPosZ);
		gl.glVertex3f(canvasPosX - (sideSize / 2f), canvasPosY - (medianSection), canvasPosZ);
		gl.glEnd();
	}

	@Override
	public Point3D getPosition() {
		return centralPoint;
	}

	@Override
	public void setPosition(Point3D position) {
		centralPoint = position;
		canvasPosX = (float)  centralPoint.getX();
		canvasPosY = (float) centralPoint.getY();
		canvasPosZ = (float) -centralPoint.getZ();
	}

	@Override
	public float[] getPositionOnCanvas() {
		float[] canvasPosition = { canvasPosX, canvasPosY, canvasPosZ };
		return canvasPosition;
	}

	@Override
	public void translateFigure(GL2 gl, Point3D position, float offsetX, float offsetY, float offsetZ) {
		gl.glTranslatef((float) position.getX() + offsetX, (float) position.getY() + offsetY,
				(float) position.getZ() + offsetZ);
	}

	@Override
	public void setColor(float red, float green, float blue) {
		colorRGB[0] = red;
		colorRGB[1] = green;
		colorRGB[2] = blue;
	}

	@Override
	public float[] getColor() {
		return colorRGB;
	}

	@Override
	public float getRadius() {
		return radius;
	}
	
	@Override
	public int getShape() {
		return CotaFigure.TRIANGLE;
	}
}
