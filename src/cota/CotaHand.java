package cota;

import javafx.geometry.Point3D;

/**
 * Hand object
 * Represented through a circle
 * Contains the item grabbed from the bottom menu
 * 	and the position of the hand at any given time
 *
 */
public class CotaHand {

	private CotaCircle handView; // the hand circle
	private Point3D position; // actual position from Kinect

	// position on screen
	private float canvasPosX;
	private float canvasPosY;
	private float canvasPosZ;
	
	private boolean empty; // hand has grabbed an object or not
	private CotaFigure itemOnHand; // item grabbed from the bottom menu

	public CotaHand() {
		handView = null;
		itemOnHand = null;
		empty = true;
	}

	public CotaHand(Point3D position) {
		this.position = position;
		empty = false;
		setHandView(position);
	}

	private void setHandView(Point3D position) {
		handView = new CotaCircle(position, 0.1f);
	}

	public CotaFigure getHandView() {
		return handView;
	}

	public void setPosition(Point3D position) {
		this.position = position;
		canvasPosX = (float) -this.position.getX();
		canvasPosY = (float) this.position.getY();
		canvasPosZ = (float) -this.position.getZ();
		setHandView(this.position);
		if (!empty) {
			itemOnHand.setPosition(position);
		}
	}

	public Point3D getPosition() {
		return this.position;
	}

	public float[] getPositionOnCanvas() {
		float[] canvasPosition = { canvasPosX, canvasPosY, canvasPosZ };
		return canvasPosition;
	}

	public boolean isEmpty() {
		return empty;
	}

	public CotaFigure getItemOnHand() {
		return itemOnHand;
	}

	public void setItemOnHand(CotaFigure item) {
		itemOnHand = item;
		empty = false;

	}

	public void removeItemOnHand() {
		itemOnHand = null;
		empty = true;
	}
}